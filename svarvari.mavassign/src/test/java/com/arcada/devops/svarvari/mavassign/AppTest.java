package com.arcada.devops.svarvari.mavassign;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void fetchFriendsTests()
    {
    	//THINGS I'D WANT TO TEST FOR:
    	//when giving a url, does it return a valid array?
    	//when giving it bogus input, does it handle the error?
    	//
    	if(App.FetchFriends("url") instanceof String[])
    	{
    		assertTrue( true );
    	}
    	
    	String [] friends = App.FetchFriends("url");
    	if(friends[0] == null)
    	{
    		assertTrue( false );
    	}
    	else if(friends[0].length() < 1)
    	{
    		assertTrue( false );
    	}
    	
    }
    
    public void urlTests()
    {
    	//i'd probably want to "spoof" the scanner input somehow?
    	//A quick online search suggests rewriting the user input(scanner) by creating a wrapper for system in/out.
    	//for the sake of saving time I won't look into that, but it would allow me to run different urls through the
    	//input below to test and see its all OK
    	String url = App.getUrl();
    	
    	if(url == null)
    	{
    		assertTrue( false );

    	}
    	else if(url.length() < 12 )
    	{
    		//url can't be shorter than "facebook.com"
    		assertTrue( false );
    	}
    }
}
