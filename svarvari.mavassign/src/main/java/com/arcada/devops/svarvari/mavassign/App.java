package com.arcada.devops.svarvari.mavassign;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static final void main( String[] args )
    {
    	String url = getUrl();
    	String [] friends = FetchFriends(url);
        System.out.println(Arrays.toString(friends));
    }
    
    //function to get url
    public static String getUrl()
    {
    	Scanner input = new Scanner(System.in);
    	System.out.println("Enter Faceboom URL:");
    	String url = input.nextLine();
    	input.close();
    	return url;
    }
    
    //function to get friends
    public static String[] FetchFriends(String url)
    {
    	//PRETEND FUNCTION:
    	//Lets pretend we are fetching the friendslist from given url
    	String [] friends = {"Steve", "Bob", "Alex"};
    	return friends;
    }
}
